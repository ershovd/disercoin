﻿using System;
using Disercoin.Core.Models;

namespace Disercoin.ConsoleApp
{
    class Program
    {
        static double sampleWinRatio = 0.8;
        static int sampleNumberoOfUserSpins = 1000;

        static void Main(string[] args)
        {
            // NumberOscillator tests 

            SimpleNumberOscillator();

            OutputLengthOf1();

            LargeRange();

            LargeList();

            return;

            // Roullete tests

            SimpleBetOnSingleNumber();
            
            BetOnSeveralNumers();
            
            BetOnAllWith2SpecificNumber();
            
            HighWinRatioBetOnSeveralNumers();
            
            LowWinRatioBetOnSeveralNumers();

            Bet13On2Numbers();

            BetOnAllAndOneNumberFAilCase();

            BetNothingFailCase();

            HouseLosesWinRatio1And4BetOnSeveral();
        }

        public static void SimpleNumberOscillator()
        {
            Console.WriteLine("SimpleNumberOscillator");
            var total = 60;
            var lower = 1;
            var upper = 10;
            var lengthList = 10;
            var list = NumberOscillator.Generate(total, lower, upper, lengthList);
            WinningTester.CalculateNumberOscillator(list, total);
        }

        public static void OutputLengthOf1()
        {
            Console.WriteLine("OutputLengthOf1");
            var total = 6;
            var lower = 1;
            var upper = 10;
            var lengthList = 1;
            var list = NumberOscillator.Generate(total, lower, upper, lengthList);
            WinningTester.CalculateNumberOscillator(list, total);
        }

        public static void LargeRange()
        {
            Console.WriteLine("LargeRange");
            var total = 75426;
            var lower = 1;
            var upper = 100000;
            var lengthList = 20;
            var list = NumberOscillator.Generate(total, lower, upper, lengthList);
            WinningTester.CalculateNumberOscillator(list, total);
        }

        public static void LargeList()
        {
            Console.WriteLine("LargeList");
            var total = 4000385;
            var lower = 1;
            var upper = 100;
            var lengthList = 500000;
            var list = NumberOscillator.Generate(total, lower, upper, lengthList);
            WinningTester.CalculateNumberOscillator(list, total);
        }


        public static void SimpleBetOnSingleNumber()
        {
            Console.WriteLine("SimpleBetOnSingleNumber");
            var userBet = new UserBet();
            userBet.PlaceBet(3, 4);
            WinningTester.CalculateWinsAndStats(userBet, sampleWinRatio, sampleNumberoOfUserSpins);
        }

        public static void HouseLosesWinRatio1And4BetOnSeveral()
        {
            Console.WriteLine("HouseLosesWinRatio1And4BetOnSeveral");
            var userBet = new UserBet();
            userBet.PlaceBet(3, 5);
            userBet.PlaceBet(6, 2);            
            WinningTester.CalculateWinsAndStats(userBet, 1.4, 4000);
        }

        public static void HighWinRatioBetOnSeveralNumers()
        {
            Console.WriteLine("HighWinRatioBetOnSeveralNumers");
            var userBet = new UserBet();
            userBet.PlaceBet(3, 7);
            userBet.PlaceBet(6, 8);
            userBet.PlaceBet(3, 1);
            WinningTester.CalculateWinsAndStats(userBet, 0.95, sampleNumberoOfUserSpins);
        }

        public static void LowWinRatioBetOnSeveralNumers()
        {
            Console.WriteLine("LowWinRatioBetOnSeveralNumers");
            var userBet = new UserBet();
            userBet.PlaceBet(3, 7);
            userBet.PlaceBet(6, 8);
            userBet.PlaceBet(3, 1);
            WinningTester.CalculateWinsAndStats(userBet, 0.55, sampleNumberoOfUserSpins);
        }

        public static void BetOnSeveralNumers()
        {
            Console.WriteLine("BetOnSeveralNumers");
            var userBet = new UserBet();
            userBet.PlaceBet(4, 5);
            userBet.PlaceBet(1, 10);
            userBet.PlaceBet(3, 3);
            WinningTester.CalculateWinsAndStats(userBet, sampleWinRatio, sampleNumberoOfUserSpins);
        }

        public static void BetOnAllEvenFAilCase()
        {
            Console.WriteLine("BetOnAllEven - Fail Case");
            var userBet = new UserBet();
            for (int i = 0; i < Configuration.TotallRoulletteNumber; i++)
            {
                userBet.PlaceBet(1, i);
            }

            WinningTester.CalculateWinsAndStats(userBet, sampleWinRatio, sampleNumberoOfUserSpins);
        }

        public static void BetOnAllAndOneNumberFAilCase()
        {
            Console.WriteLine("BetOnAllAndOneNumber - Fail Case");
            var userBet = new UserBet();
            for (int i = 0; i < Configuration.TotallRoulletteNumber; i++)
            {
                if (i == 3)
                {
                    userBet.PlaceBet(2, i);
                    continue;
                }
                userBet.PlaceBet(1, i);
            }

            WinningTester.CalculateWinsAndStats(userBet, sampleWinRatio, sampleNumberoOfUserSpins);
        }

        public static void BetNothingFailCase()
        {
            Console.WriteLine("BetNothing- Fail Case");
            var userBet = new UserBet();
            WinningTester.CalculateWinsAndStats(userBet, sampleWinRatio, sampleNumberoOfUserSpins);
        }

        public static void Bet13On2Numbers()
        {
            Console.WriteLine("Bet13On2Numbers");
            var userBet = new UserBet();
            userBet.PlaceBet(13, 2);
            userBet.PlaceBet(13, 5);
            WinningTester.CalculateWinsAndStats(userBet, sampleWinRatio, sampleNumberoOfUserSpins);
        }

        public static void BetOnAllWith2SpecificNumber()
        {
            Console.WriteLine("BetOnAllWith2SpecificNumber");
            var userBet = new UserBet();
            for (int i = 0; i < Configuration.TotallRoulletteNumber; i++)
            {
                if (i == 3)
                {
                    userBet.PlaceBet(10, 3);
                    userBet.PlaceBet(4, 6);
                    continue;
                }

                userBet.PlaceBet(1, i);
            }

            WinningTester.CalculateWinsAndStats(userBet, sampleWinRatio, sampleNumberoOfUserSpins);
        }
    }
}
