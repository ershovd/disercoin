﻿namespace Disercoin.Core.Models
{
    /// <summary>
    /// Default configuration used by roullete class
    /// </summary>
    public static class Configuration
    {
        /// <summary>
        /// Total numbers in roullete
        /// </summary>
        public static int TotallRoulletteNumber { get; } = 13;
        /// <summary>
        /// 
        /// </summary>
        public static int PrizeTierMultiplier { get; } = 5;
        /// <summary>
        /// Number of spins to calculate pseudo-random roullete based on winning ratio
        /// </summary>
        public static int NumberOfSpinsForPseudoRandom { get; } = 5000;        
        /// <summary>
        /// Amount of payout be 1 bet if wins
        /// </summary>
        public static int PayoutByBet { get; } = TotallRoulletteNumber;        
    }
}
