﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Disercoin.Core.Models
{
    /// <summary>
    /// Generate list of pseudo-random numbers with fixed sum
    /// </summary>
    public static class NumberOscillator
    {
        private static Random rand = new Random();

        /// <summary>
        /// Generates list of numbers with <paramref name="outputLength"></paramref> that are within inclusive <paramref name="lower"></paramref>
        /// and <paramref name="upper"></paramref and sum equal <param name="total"></param>        
        /// </summary>
        /// <param name="total">Total required sum of numbers</param>
        /// <param name="lower">Lower numbers limit</param>
        /// <param name="upper">Upper numbers limit</param>
        /// <param name="outputLength">Legth of output list</param>
        /// <returns></returns>
        public static List<int> Generate(int total, int lower, int upper, int outputLength)
        {
            if (lower <= 0 || upper <= 0 || total <= 0 || outputLength <= 0)
                throw new ArgumentOutOfRangeException("Input parameters should be positive numbers");
            if (upper <= lower)
                throw new ArgumentException("Upper limit should be greater than lower limit");
            if (total <= lower * outputLength)
                throw new ArgumentException("Total parameter should be greater than lower limit times outputLength");
            if (total >= upper * outputLength)
                throw new ArgumentException("Total parameter should be less than upper limit times outputLength");


            var resultArr = CreateArrayWithWeigthAverage(total, lower, upper, outputLength);

            PadArrayToMatchLength(resultArr, total, lower, upper);

            return resultArr.ToList();
        }
        /// <summary>
        /// Creates pseudo-random array, adjusted by average
        /// </summary>
        private static int[] CreateArrayWithWeigthAverage(int total, int lower, int upper, int outputLength)
        {
            double avgPerStep = total / (double)outputLength;

            int[] resultArr = new int[outputLength];
            // if true, next generated should be between lower and current generated number
            bool? shouldBeInLowRange = null;
            for (int i = 0; i < outputLength; i++)
            {
                int adjustedLower, adjustedUpper;

                adjustedLower = lower;
                adjustedUpper = upper;

                if (shouldBeInLowRange != null)
                {
                    if (shouldBeInLowRange.Value)
                    {
                        adjustedUpper = (int)avgPerStep;
                    }
                    else
                    {
                        adjustedLower = (int)avgPerStep;
                    }
                }

                int generated = rand.Next(adjustedLower, adjustedUpper);

                if (generated > avgPerStep)
                {
                    shouldBeInLowRange = true;
                }
                else if (generated < avgPerStep)
                {
                    shouldBeInLowRange = false;
                }
                else // newgen == avgPerStep
                {
                    shouldBeInLowRange = null;
                }
                resultArr[i] = generated;
            }
            return resultArr;
        }

        /// <summary>
        /// Pad array values with +1 or -1 to match total
        /// </summary>
        private static void PadArrayToMatchLength(int[] resulArr, int total, int lower, int upper)
        {
            var totalDiff = total - resulArr.Sum();
            if (totalDiff != 0)
            {
                var addToItem = totalDiff > 0 ? 1 : -1;
                for (int i = 0; i < Math.Abs(totalDiff); i++)
                {
                    int item;
                    int index;
                    // get index randomly to even distrbution
                    do
                    {
                        index = rand.Next(0, resulArr.Length);
                        item = resulArr[index] + addToItem;
                    }
                    while ((item > upper) || (item < lower));

                    resulArr[index] = item;
                }
            }
        }
    }
}
