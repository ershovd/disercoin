﻿using System;
using System.Collections.Generic;

namespace Disercoin.Core.Models
{
    public class Roullete
    {
        private static Random random = new Random();
        /// <summary>
        /// Storing number of winning and not winning numbers of roullete
        /// </summary>
        private static int[] winningNumbersArray = new int[Configuration.NumberOfSpinsForPseudoRandom];

        /// <summary>
        /// Stores indices already used in current userbet session
        /// </summary>
        private static List<int> usedArrayIndices = new List<int>();

        /// <summary>
        /// Generates pseudo random spin with user bet custom
        /// </summary>
        /// <returns></returns>
        public static int SpinWithWinningRatio()
        {
            int arrIndex;
            if (usedArrayIndices.Count == Configuration.NumberOfSpinsForPseudoRandom)
            {
                usedArrayIndices.Clear();
            }
            do
            {
                arrIndex = random.Next(Configuration.NumberOfSpinsForPseudoRandom);
            }
            while (usedArrayIndices.Contains(arrIndex));

            var spinWithWinRatio = winningNumbersArray[arrIndex];
            usedArrayIndices.Add(arrIndex);

            return spinWithWinRatio;
        }


        /// <summary>
        /// Returns true if bets are evenly distrubuted on each number
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private static bool BetsAreEvenOnEachNumber(UserBet user)
        {
            var userNumbers = user.UserBetNumbers;
            // bet nothing on each number
            if (userNumbers == null || userNumbers.Count == 0)
            {
                return true;
            }
            var userNonBetNumbers = user.UserNonBetNumbers;
            // bet 1 on each number
            if (userNonBetNumbers.Count == 0 && user.TotalBetPlaced == Configuration.TotallRoulletteNumber)
            {
                return true;
            }

            // check if all number are evenly distributed
            var maxDivider = user.TotalBetPlaced / Configuration.TotallRoulletteNumber;
            if (maxDivider > 0)
            {
                var remainder = user.TotalBetPlaced % (Configuration.TotallRoulletteNumber * maxDivider);
                if (remainder == 0)
                {
                    var userNewNumbers = NormalizeBetNumbers(user);
                    if (userNewNumbers.Count == 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }


        /// <summary>
        /// Initializes inner arrays for generating pseudo-random spins based on <paramref name="user"/> current bet and <paramref name="userWinningRatio"/>
        /// </summary>
        /// <param name="user">User bet instance with bets speciied</param>
        /// <param name="userWinningRatio">Ratio of user winning</param>
        public static void InitByPlayerAndWinRatio(UserBet user, double userWinningRatio)
        {
            if (userWinningRatio < 0)
            {
                throw new ArgumentOutOfRangeException("Winning ration should be > 0");
            }

            if (userWinningRatio > 1)
            {
                // uncomment of you want userWinningRatio witinn valid range
                //throw new ArgumentOutOfRangeException("Winning ratio should be between 0 and 1");
            }

            var betMultiplierCoeff = user.TotalBetPlaced * userWinningRatio / Configuration.TotallRoulletteNumber;

            if (betMultiplierCoeff < 1)
            {
                // uncomment if you want to throw when it is impossible to match userWinningRatio
                //throw new ArgumentException($"With total bet placed {user.TotalBetPlaced} it is impossible to match win ratio: {userWinningRatio}");
            }

            var defaultNumberOfWinCasePerWinNumber = (Configuration.NumberOfSpinsForPseudoRandom * userWinningRatio / Configuration.PayoutByBet);

            var userNumbers = user.UserBetNumbers;
            if (BetsAreEvenOnEachNumber(user))
            {
                FillResultsArrayWithRandom(0, new List<int>());
                return;
            }
            
            var isCustomWinNumberCases = false;
            int numberOfWinCases = 0;

            // if placed on each number and not evenly
            if (user.UserNonBetNumbers.Count == 0)
            {
                userNumbers = NormalizeBetNumbers(user);

                // has solution to win such amount of bets, 
                if (betMultiplierCoeff >= 1)
                {
                    isCustomWinNumberCases = true;
                    double sum = (betMultiplierCoeff - 1) * Configuration.NumberOfSpinsForPseudoRandom;


                    int totalAdjustedBet = 0;
                    for (int i = 0; i < userNumbers.Count; i++)
                    {
                        totalAdjustedBet += user.Bets[userNumbers[i]] - 1;
                    }
                    numberOfWinCases = (int)sum / totalAdjustedBet;
                }
                // otherwise has no solution
                else
                {
                    // uncomment of you want throw when impossible to match user winning ratio
                    //throw new Exception();
                }                
            }

            
            winningNumbersArray = new int[Configuration.NumberOfSpinsForPseudoRandom];
            int currentIndex = 0;
            int totalNumberOfWinCases = isCustomWinNumberCases ? numberOfWinCases
                : (int)defaultNumberOfWinCasePerWinNumber;
            
            foreach (var userNumber in userNumbers)
            {
                for (int j = 0; j < totalNumberOfWinCases; j++)
                {
                    winningNumbersArray[currentIndex] = userNumber;
                    currentIndex++;
                }
            }

            FillResultsArrayWithRandom(currentIndex, userNumbers);
        }

        /// <summary>
        /// Normalize user bet on each number by substracting 1 from each bet
        /// </summary>
        /// <param name="user"></param>
        /// <returns>List of user bet number </returns>
        private static IList<int> NormalizeBetNumbers(UserBet user)
        {
            var newUser = user.Copy();            

            while (newUser.UserNonBetNumbers.Count == 0 && newUser.UserBetNumbers.Count != 0)
            {
                for (int i = 0; i < newUser.Bets.Count; i++)
                {
                    newUser.PlaceBet(-1, i);
                }
            }

            return newUser.UserBetNumbers;
        }

        /// <summary>
        /// Fill the rest of winningNumbersArray array with random numbers missing in <paramref name="numbersToSkip"/>, starting from <paramref name="startIndex"/> 
        /// </summary>
        /// <param name="startIndex"></param>
        /// <param name="numbersToSkip"></param>
        private static void FillResultsArrayWithRandom(int startIndex, IList<int> numbersToSkip)
        {
            if (startIndex > Configuration.NumberOfSpinsForPseudoRandom)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (numbersToSkip == null)
            {
                throw new ArgumentNullException();
            }

            for (int i = startIndex; i < Configuration.NumberOfSpinsForPseudoRandom; i++)
            {
                int nonWinNumber;
                do
                {
                    nonWinNumber = RandomSpin();
                }
                while (numbersToSkip.Contains(nonWinNumber));
                winningNumbersArray[i] = nonWinNumber;
            }
        }

        /// <summary>
        /// "True" random spin for 0 to maximum number of roullete 
        /// </summary>       
        public static int RandomSpin()
        {
            return random.Next(Configuration.TotallRoulletteNumber - 1);
        }
    }
}
