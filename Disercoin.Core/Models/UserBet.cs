﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Disercoin.Core.Models
{
    /// <summary>
    /// Handles setting and configuring user bet
    /// </summary>
    public class UserBet : ICloneable
    {
        public UserBet()
        {
            Bets = new List<int>();
            for (int i = 0; i < Configuration.TotallRoulletteNumber; i++)
            {
                Bets.Add(0);
            }
        }

        /// <summary>
        /// Deep copy user bet
        /// </summary>
        /// <returns></returns>
        public UserBet Copy()
        {
            var newBet = new UserBet();
            newBet.Bets = this.Bets.ToList();
            newBet.UserBetNumbers = this.UserBetNumbers.ToList();
            newBet.UserNonBetNumbers = this.UserNonBetNumbers.ToList();
            return newBet;
        }

        public object Clone()
        {
            return Copy();
        }


        /// <summary>
        /// Place specified <paramref name="amount"/> on particular Roullete <paramref name="number"/>
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="number"></param>
        public void PlaceBet(int amount, int number)
        {
            Bets[number] += amount;

            UserNonBetNumbers = new List<int>();
            UserBetNumbers = new List<int>();
            for (int i = 0; i < Bets.Count; i++)
            {
                int bet = Bets[i];
                if (bet > 0)
                {
                    UserBetNumbers.Add(i);
                }
                else
                {
                    UserNonBetNumbers.Add(i);
                }
            }
        }


        /// <summary>
        /// Combintation of bet on specific numbers
        /// </summary>
        public IList<int> Bets { get; private set; }

        /// <summary>
        /// Total sum of all user bet
        /// </summary>
        public int TotalBetPlaced
        {
            get { return Bets.Sum(); }
        }

        /// <summary>
        /// List of prizes by numbers
        /// </summary>
        public IList<PrizeTier> BetsByPrizeTier
        {
            get
            {
                var firstThreshold = (int)PrizeTier.small * Configuration.PrizeTierMultiplier;
                var secondThreshold = (int)PrizeTier.meduim * Configuration.PrizeTierMultiplier;
                var thirdThreshold = (int)PrizeTier.large * Configuration.PrizeTierMultiplier;

                return Bets.Select(f =>
                {
                    if (f <= (firstThreshold))
                    {
                        return PrizeTier.small;
                    }
                    else if (f <= secondThreshold)
                    {
                        return PrizeTier.meduim;
                    }
                    else 
                    {
                        return PrizeTier.large;
                    }                    

                }).ToList();
            }
        }

        /// <summary>
        /// List of numbers that users has NOT placed bet
        /// </summary>
        public IList<int> UserNonBetNumbers
        {
            get; private set;
        }

        /// <summary>
        /// List of numbers that users has placed bet
        /// </summary>
        public IList<int> UserBetNumbers
        {
            get; private set;
        }

        /// <summary>
        /// Get the amount that user wins on particular roullete number
        /// </summary>
        /// <param name="roulleteNumber"></param>
        /// <returns></returns>
        public int WinAmount(int roulleteNumber)
        {
            if (roulleteNumber > Configuration.TotallRoulletteNumber)
                throw new ArgumentOutOfRangeException();

            return Bets[roulleteNumber] * Configuration.PayoutByBet;
        }
    }
    /// <summary>
    /// Tiers of prizes threshold
    /// </summary>
    public enum PrizeTier
    {
        small = 1,
        meduim = 3,
        large = 6,
    }
}
