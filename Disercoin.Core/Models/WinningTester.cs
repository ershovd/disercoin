﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Disercoin.Core.Models
{
    /// <summary>
    /// Test class showcasing using Roullete
    /// </summary>
    public class WinningTester
    {
        public static void CalculateWinsAndStats(UserBet user, double userWinRatio, int numberOfSpins = 5000)
        {
            int totalBetPlaced = user.TotalBetPlaced * numberOfSpins;
            int totalNumberOfWins = 0;
            int totalPlayerBetWon = 0;
            Roullete.InitByPlayerAndWinRatio(user, userWinRatio);
            for (int i = 0; i < numberOfSpins; i++)
            {
                var result = Roullete.SpinWithWinningRatio();
                var currentWin = user.WinAmount(result);
                if (currentWin > 0)
                {
                    totalNumberOfWins++;
                    totalPlayerBetWon += currentWin;
                }
            }
            int totalHouseBetWon = totalBetPlaced - totalPlayerBetWon;
            var userWinRation = totalPlayerBetWon / (double)totalBetPlaced;

            Console.WriteLine($"Number of spins: {numberOfSpins};  User total bet: {user.TotalBetPlaced}; Win Parameter ratio: {userWinRatio}");
            Console.WriteLine($"User win bets: {totalPlayerBetWon}; House wins bets: {totalHouseBetWon}");
            Console.WriteLine($"Actual user win ratio: {userWinRation}");
            Console.WriteLine();
        }


        public static void CalculateNumberOscillator(List<int> list, int total)
        {
            var actualTotal = list.Sum();

            Console.WriteLine($"Parameter total: {total}; Actual total: {actualTotal}");
            Console.WriteLine();
        }
    }
}
